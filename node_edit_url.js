/**
 * @author mikeker
 */
(function ($) {
  Drupal.behaviors.better_exposed_filters = {
    attach: function() {
      // Add a generate new salt option
      $('<a href="" class="button">Generate new salt</a>')
        .click(function(e) {    // Mirrors _neu_generate_salt()
          // Build salt: leave out one and lowercase-L, zero and capital-O due to confusion.
          var possible = '23456789abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ!@#$%&():;.,<>';
          var salt = '';
          for ($i = 0; $i < 16; $i++) {
            salt += possible.substr(Math.floor(Math.random() * possible.length), 1);
          }

          $('#edit-node-edit-url-salt').val(salt);

          // Don't follow the link...
          return false;
        })
        // Place in DOM
        .insertAfter('#edit-node-edit-url-salt')
      ;
    }                   // attach: function() {
  };                    // Drupal.behaviors.better_exposed_filters = {
}) (jQuery);
